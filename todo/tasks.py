import traceback
import sys

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from django.conf import settings

from django.shortcuts import get_object_or_404

from todo.models import Todo,CustomUser
from django.contrib.auth.models import User
from celery import task

from myproject.libs.utils import send_email

@task
def send_email_alert_to_users(user_id):

    subject_template_name = "send_alert_subject.txt"
    html_body_template_name = "send_email_alert.html"
    print(user_id)
    todos = Todo.objects.filter(user__id=user_id)
    print(todos)
    count = todos.count
    
    for todo in todos:
        context = {
            "action_name": "Daily",
            "todo": todo.id,
            "username": todo.user.username,
            "date_due": todo.date_due,
            "user_email": todo.user.email,
            "task_title": todo.title,
            "task_text": todo.text,
            "task_priority": todo.priority,
            "count":count,
            "subject_template_name": subject_template_name,
            "html_body_template_name": html_body_template_name,
            "todo_list": "http://127.0.0.1:8000/list/",
            
        }
        print(context)
        schedule = Todo.date_due
        send_email(**context)
     



from django.urls import path
from django.conf.urls import url
from todo import views
from todo.views import index
urlpatterns = [
    path('',views.index,name='index'),
    path('add/', views.addTodo, name='add'),
    path('all/',views.deleteAll,name='all'),
    path('delete/<todo_id>',views.deleteTodo,name='delete'),
    path('info/<todo_id>',views.info,name='info'),
    path('list/',views.list,name="list"),

]

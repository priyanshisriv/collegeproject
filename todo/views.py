import boto3
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from todo.models import Todo,CustomUser
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.conf import settings
from myproject.libs.utils import send_email
from myproject.libs.sms import Sms
from django.views.generic.list import ListView
from django.views.generic import View
from django.contrib.auth import login,authenticate
from django.contrib import messages
from todo.forms import Form,UserRegisterForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.http import HttpResponse, HttpResponseRedirect


def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            messages.success(request,f'Your account has been created.You are now able to log in')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request,"todo/register.html",{"form":form})

def Login_View(request):
    if request.method=='POST':
        username= request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('index')
    context = {}
    return render(request, 'todo/login.html', context)

@login_required(login_url = '/login/')
def index(request):
    todo_list = Todo.objects.order_by('id')

    form = Form()

    context = {'todo_list' : todo_list, 'form' : form}

    return render(request, 'todo/index.html', context)

def list(request):
    user_id=request.user.id
    todo_list = Todo.objects.filter(user__id=user_id)
    context = {'todo_list':todo_list}
    return render(request,'todo/list.html',context)


@require_POST
def addTodo(request):
    form = Form(request.POST)

    if form.is_valid():
        user=CustomUser.objects.get(id=request.user.id)
        todo = Todo(title=request.POST['title'],priority=request.POST['priority'],email_alert=request.POST.get('email_alert'),date_due=request.POST['date_due'],text=request.POST['text'],user=user)     
        todo.save()
        template_name = "todo/sms.txt"
        ctx = {
        'name': user.username,
        'message': "You have recently added a task to your todo list,",
        'title':todo.title,
        'alert':todo.date_due,
        } 
        sms = Sms(topic=ctx['name'], template_name=template_name)
        sms.send_sms(user.contact, ctx)
        print("sms has been sent")
        '''
        subject_template_name = "send_alert_subject.txt"
        html_body_template_name = "send_email_alert.html"
        context = {
            "todo": todo.id,
            "username": user.username,
            "date_due": todo.date_due,
            "user_email": user.email,
            "task_title": todo.title,
            "task_text": todo.text,
            "task_priority": todo.priority,
            "subject_template_name": subject_template_name,
            "html_body_template_name": html_body_template_name,
            "todo_list": "http://127.0.0.1:8000/list/",
            
        }
        schedule = Todo.date_due
        send_email(**context)
        '''
    return redirect('list')

def deleteAll(request):
    user_id = request.user.id
    del_all = Todo.objects.filter(user__id=user_id)
    del_all.delete()
    return redirect('list')

def deleteTodo(request,todo_id):
    del_todo = Todo.objects.get(pk=todo_id).delete()
    return redirect('list')

def info(request,todo_id):

    todo_info = Todo.objects.get(pk=todo_id)
    context = {'title' : todo_info.title,
                'text':todo_info.text,
                'priority':todo_info.priority,
                'date':todo_info.date_due,
                'complete':todo_info.complete,
    }
    return render(request, 'todo/info.html', context)

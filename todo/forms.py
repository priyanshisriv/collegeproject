from django import forms 
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from todo.models import Todo,CustomUser
from datetime import datetime

class Form(forms.ModelForm):
    TRUE = "True"
    FALSE= "False"

    ALERT = (
        (TRUE, "True"),
        (FALSE, "False")
    )     
    priority=forms.RadioSelect() 
    email_alert=forms.ChoiceField(choices=ALERT)
    class Meta:
        model = Todo
        fields = ['title','text','date_due','priority','email_alert','complete','user',]
        widgets = {
           'date_due': forms.DateTimeInput(attrs={'class': 'datepicker','placeholder' : 'Enter due date', 'aria-label' : 'Todo', 'aria-describedby' : 'add-btn'}),
           'title':forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Enter Task Title e.g. Delete junk files', 'aria-label' : 'Todo', 'aria-describedby' : 'add-btn'}),
           'text':forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Enter Description', 'aria-label' : 'Todo', 'aria-describedby' : 'add-btn'}),
        }

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = CustomUser
        widgets = {
        'password': forms.PasswordInput(),
        }
        fields = ['username','email','contact','password1','password2',]


class UserloginForm(AuthenticationForm):

    class Meta:
        model = CustomUser
        widgets = {
        'password': forms.PasswordInput(),}
        fields = ['email','password',]


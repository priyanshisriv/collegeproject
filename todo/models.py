from django.db import models
from datetime import datetime
from datetime import date

from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User,AbstractUser

class CustomUser(AbstractUser):
    contact = PhoneNumberField(unique=True,blank=True,default='+91',null=True,help_text="Enter Contact Number")
    email = models.EmailField(unique=True)
class Todo(models.Model):
    '''model for todo app'''

    HIGH = "High"
    LOW = "Low"
    MEDIUM = "Medium"

    PRIORITY = (
        (HIGH, "High"),
        (LOW, "Low"),
        (MEDIUM, "Medium")
    )
    
    email_alert = models.BooleanField(default=False)
    complete = models.BooleanField(default=False)
    title = models.CharField(max_length=50,default="")
    text = models.TextField(null=True,blank=True)
    date_due = models.DateTimeField(default=datetime.now)
    priority = models.CharField(choices=PRIORITY,max_length=10,default=LOW)
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE,null=True,blank=True,related_name="todo_user")

    class Meta:
        verbose_name = _('Todo')
        verbose_name_plural = _('Todos')
  
    def __str__(self):
        return self.title

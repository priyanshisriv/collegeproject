from django.contrib import admin

from todo.models import Todo,CustomUser

admin.site.register(Todo)
admin.site.register(CustomUser)

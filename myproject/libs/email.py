from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from .utils import send_email
from django.contrib.auth.models import User


def send_business_owner_reviews_reports(report_name, file, content_type, user):
    """sending different types of reports to the business owner"""

    subject = "send_email_subject.txt"
    html_body = "send_email_alert.html"
    template_context = {
        'filename': report_name,
        'content_type': content_type,
        "subject_template_name": subject,
        "html_body_template_name": html_body,
        "user_email": User.email,
        "user_name": User.username,
        "file": file
    }
    send_email(**template_context)




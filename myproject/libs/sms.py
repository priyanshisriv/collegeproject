#https://bradmontgomery.net/blog/sending-sms-messages-amazon-sns-and-python/

import boto3
from django.conf import settings
from django.template.loader import render_to_string
import requests
import json


class Sms:

    def __init__(self, topic, template_name):

        self.client = boto3.client(
            "sns",
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
            region_name=settings.AWS_SES_REGION_NAME
        )
        self.topic = topic
        self.template_name = template_name
        self.topic_arn = None


    def send_sms(self, number, context):
        message = render_to_string(self.template_name, context)
        self.client.publish(Message=message, PhoneNumber=str(number))

import io
import csv
import requests
import re

from datetime import timedelta, date, datetime
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.utils import timezone

from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMultiAlternatives,send_mail


def send_email(**ctx):
    subject = render_to_string(ctx["subject_template_name"], ctx)
    subject = ''.join(subject.splitlines())
    cc = ctx['cc'] if ctx.get('cc') else []
    htmly = get_template(ctx['html_body_template_name'])
    html_content = htmly.render(ctx)
    default_email = settings.DEFAULT_FROM_EMAIL
    msg = EmailMultiAlternatives(
            subject,
            html_content,
            to=[ctx['user_email']],
            from_email=default_email,
            reply_to=None,
            )
    print(msg)
    msg.attach_alternative(html_content, "text/html")
    try:
        msg.send(fail_silently=True)
        print("email sent!")
    except:
        logger.error("Unable to send mail.")
    
    




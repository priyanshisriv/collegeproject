import json

from django.contrib.gis.geos import GEOSGeometry, GEOSException
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.exceptions import ValidationError
from rest_framework_gis.serializers import GeometryField
from rest_framework import serializers

from fbs_project.apps.accounts.models import ContactList, User
from fbs_project.apps.campaign.models import Template
from fbs_project.apps.business.models import Links


class GeometryPointFieldSerializerFields(GeometryField):

    def to_internal_value(self, value):
        try:
            value = value.split(",")
        except:
            raise ValidationError(
                _("Enter the co-ordinates in (latitude,longitude). Ex-12,13")
            )
        try:
            latitude = float(value[0])
        except ValueError:
            raise ValidationError(
                _("Enter the co-ordinates in (latitude,longitude). Ex-12,13")
            )
        try:
            longitude = float(value[1])
        except ValueError:
            raise ValidationError(
                _("Enter the co-ordinates in (latitude,longitude). Ex-12,13")
            )
        value = {
            "type": "Point",
            "coordinates": [latitude, longitude]
        }
        value = json.dumps(value)
        try:
            return GEOSGeometry(value)
        except (ValueError, GEOSException, TypeError):
            raise ValidationError(
                _('Invalid format: string or unicode input unrecognized as GeoJSON, WKT EWKT or HEXEWKB.'))

    def to_representation(self, value):
        """ """
        value = super(
            GeometryPointFieldSerializerFields, self).to_representation(value)
        # change to compatible with google map
        data = "," .join([str(value['coordinates'][0]), str(value['coordinates'][1])])
        return data



class ForeignKeyField(serializers.RelatedField):
    """ cutom ForeignKey fields """

    def __init__(self, *args, **kwargs):

        self.__display_name = kwargs.pop('display_name', None)
        super(ForeignKeyField, self).__init__(*args, **kwargs)

    def get_queryset(self):
        """ if query set is function then call this function """
        if callable(self.queryset):
            return self.queryset(self.context['request'])
        else:
            return self.queryset

    def to_internal_value(self, value):
            try:
                return self.get_queryset().get(id=value)
            except ObjectDoesNotExist:
                raise ValidationError({'error': "Invalid id"})
    def to_representation(self, value):
        if self.__display_name:
          return getattr(value, self.__display_name)
        else:
            return value.id



class ReviewLinksSerializerFields(serializers.RelatedField):
    """
    A custom field to use for the review links generic relationship.
    """

    def get_queryset(self):
        return Links.objects.all()

    def to_internal_value(self, value):
        """ get the input of the links objects"""

        try:
            links = Links.objects.get(id=value)
        except Links.DoesNotExist:
            raise exceptions.ParseError({'error': "Invalid data"})
        return links

    def to_representation(self, value):
        """
        Serialize link objects to a simple textual representation.
        """
        return {'link': value.link, 'web_portal': value.web_portal.provider}

class ContactSerializerField(serializers.PrimaryKeyRelatedField):
    """ """
    def to_representation(self, value):
        value = super().to_representation(value)
        contact = ContactList.objects.get(id=value)
        return {'id': contact.id, 'name': contact.name}


class TemplateSerializerField(serializers.PrimaryKeyRelatedField):
    def to_representation(self, value):
        value = super().to_representation(value)
        temp = Template.objects.get(id=value)
        return {'id': temp.id, 'name': temp.name}


class UserSerializerField(serializers.PrimaryKeyRelatedField):
    def to_representation(self, value):
        value = super().to_representation(value)
        user = User.objects.get(id=value)
        return {'id': user.id, 'email': user.email, 'name':user.get_full_name()}

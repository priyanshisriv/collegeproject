""" custom module pipeline for the user social registraion."""

import hashlib
import urllib
import requests
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

from accounts.models import User

USER_FIELDS = ['email']

class SocialRegistrationEmailExceptions(Exception):
    pass

def auto_logout(*args, **kwargs):

    """ Do not compare current user with new one """
    return {'user': None}


def create_user(strategy, details, backend, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}
    fields = dict((name, kwargs.get(name, details.get(name)))
                  for name in backend.setting('USER_FIELDS', USER_FIELDS))
    fields['is_active'] = True
    if not fields:
        return
    return {
        'is_new': True,
        'user': strategy.create_user(**fields)
    }


def save_avatar(strategy, details, user=None, *args, **kwargs):
    """Get user avatar from social provider."""
    if user:
        backend_name = kwargs['backend'].__class__.__name__.lower()
        response = kwargs.get('response', {})
        social_thumb = None
        if 'facebook' in backend_name:
            if 'id' in response:
                social_thumb = (
                    'http://graph.facebook.com/{0}/picture?type=normal'
                ).format(response['id'])
        elif 'twitter' in backend_name and response.get('profile_image_url'):
            social_thumb = response['profile_image_url']
        elif 'googleoauth2' in backend_name and response.get('image', {}).get('url'):
            social_thumb = response['image']['url'].split('?')[0]
        else:
            social_thumb = 'http://www.gravatar.com/avatar/'
            social_thumb += hashlib.md5(
                user.email.lower().encode('utf8')).hexdigest()
            social_thumb += '?size=100'

        r = requests.get(social_thumb)

        if r.status_code == requests.codes.ok:
            img_temp = NamedTemporaryFile(delete=True)
            img_temp.write(r.content)
            img_temp.flush()
            img_filename = urllib.parse.urlsplit(social_thumb).path[1:]
            user.avtar.save(img_filename, File(img_temp), save=True)
            strategy.storage.user.changed(user)


def check_for_email(backend, uid, user=None, *args, **kwargs):
    """ pipeline method for check user email """

    if not kwargs['details'].get('email'):
        raise SocialRegistrationEmailExceptions

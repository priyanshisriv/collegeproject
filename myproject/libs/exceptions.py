class GoogleAuthExceptions(Exception):
    
    def __init__(self, *args, **kwargs):
        super(GoogleAuthExceptions, self).__init__("Inavliad auth", *args, **kwargs)

class GoogleLocationNotFound(Exception):

    def __init__(self, *args, **kwargs):
        super(GoogleLocationNotFound, self).__init__("location not match", *args, **kwargs)

class InvalidTwitterLink(Exception):

    def __init__(self, *args, **kwargs):
        super(InvalidTwitterLink, self).__init__("invlaid twitter link", *args, **Kwargs)

class InvalidTwitterAuth(Exception):
    
    def __init__(self, *args, **kwargs):
        super(InvalidTwitterAuth, self).__init__("invalid auth token", *args, **kwargs)

class FacebookInvalidToken(Exception):
    
    def __init__(self, *args, **kwargs):
        super(FacebookInvalidToken, self).__init__("invalid facebook token ", *args, **kwargs)


class FacebookInvalidPageLink(Exception):

    def __init__(self):
        super(FacebookInvalidPageLink, self).__init__("Invalid facebook page link", *args, **kwargs)


class InvalidGoibiboLink(Exception):

    def __init__(self):
        super(InvalidGoibiboLink, self).__init__("Invalid goibibo page link", *args, **kwargs)

class InvalidBookingToken(Exception):

    def __init__(self):
        super(InvalidBookingToken, self).__init__("Invalid booking access token", *args, **kwargs)

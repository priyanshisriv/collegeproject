from rest_framework.response import Response

def choices_response(queryset, display_name='name', value='id',blank_display_name="",blank_req=True ,default=None):
    if hasattr(default, value):
        default = getattr(default, value)
    elif isinstance(default, int) or default == None:
        default = default
    else:
        raise ValueError(
            "default must have attr <{value}>, or be an integer, or be None")

    if type(queryset) == tuple or type(queryset) == list:
        choices = [{
            'value': obj[0],
            'display_name': obj[1].capitalize(),
        } for obj in queryset]
    else:
        choices = [{
                       'value': getattr(obj, value),
                       'display_name': getattr(obj, display_name).capitalize(),
                   } for obj in queryset]
    if blank_req==True:
        choices.insert(0,{'value': "", "display_name": blank_display_name})
    return Response({'default': default, 'choices': choices})

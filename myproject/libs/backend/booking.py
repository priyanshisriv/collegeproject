import datetime
import requests
from django.conf import settings
from rest_framework import exceptions

from fbs_project.libs.exceptions import InvalidBookingToken


class Booking:
    """ booking client for auth url """

    HOTELS_URL = "https://hub-api.booking.com/v1.2/hotels/"
    REVIEW_URL = "https://hub-api.booking.com/v1.2/hotels/{}/reviews"
    REVIEW_REPLY_URL = "https://hub-api.booking.com/v1.2/hotels/{}/reviews/{}"
    AUTH_TOKEN_URL = settings.SOCIAL_BOOKING_AUTH_TOKEN_URL

    def __init__(self, **kwargs):
        self.client_id = settings.SOCIAL_AUTH_BOOKING_KEY
        self.client_secret = settings.SOCIAL_AUTH_BOOKING_SECRET
        self.redirect_uri = settings.SOCIAL_AUTH_BOOKING_REDIRECT_URI
        self.auth_url = settings.SOCIAL_BOOKING_AUTH_URL.format("code", self.client_id, self.redirect_uri)
        self.link_instance = kwargs.get("link_instance", None)
        self.access_token = self.link_instance.social_details.get("access_token", None) if self.link_instance else None
        self.hotel_id = self.link_instance.social_details.get("hotel_ids") if self.link_instance else None
        self.hotel_id = self.hotel_id[0] if self.hotel_id else None

    def update_link_instance(self, social_details):
        if self.link_instance:
            self.link_instance.social_details = social_details
            self.link_instance.save()

    def get_oauth_flow_url(self):
        response = requests.get(self.auth_url)
        return response.url, None

    def exchange_token_from_code(self, code, extra_data=None):
        """ get the exchange token from the page """
        data = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "code": code,
            "redirect_uri": self.redirect_uri
        }
        response = requests.post(self.AUTH_TOKEN_URL, data)
        if self.link_instance:
            self.update_link_instance(response.json())
        return response.json()

    def format_data(self, reviews):
        """" format data to model dict """
        formatted_data = list()
        for review in reviews:
            data = dict()
            rating = review.get('ratings', 0).get("hotel_average_score")
            if float(rating) > 5.0:
                description = review.get('hotel_positive', None)
            else:
                description = review.get('hotel_negative', None)
            data['description'] = description
            data['rating'] = float(rating)
            data['reviewer'] = {
                'name': review["guest_name"]
            }
            data['review_create'] = review['created_at']
            data['extra_data'] = {
                'id': review['id'],
                "hotel_id": review['hotel_id'],
                "reservation_id": review['reservation_id']
            }
            formatted_data.append(data)
        return formatted_data

    def get_reviews(self, **kwargs):
        """ get the list of reviews for the given hotel """

        headers = {
            "X-Booking-Auth-Token": self.access_token,
        }
        response = requests.get(self.REVIEW_URL.format(self.hotel_id), headers=headers)
        if response.status_code != 200:
            return InvalidBookingToken
        reviews = response.json()['reviews']
        reviews = self.format_data(reviews)
        return reviews

    def post_comment_on_social_network(self, body, **kwargs):
        """posting comment on reviews by business owner"""
        review_id = kwargs.get("id")
        hotel_id = kwargs.get("hotel_id")

        headers = {
            "X-Booking-Auth-Token": self.access_token,
        }
        data = {"reply": body}
        response = requests.post(
            self.REVIEW_REPLY_URL.format(hotel_id, review_id),
            data=data,
            headers=headers
        )
        if response.status_code != 200:
            return InvalidBookingToken
        response.json()
        return response.json()

if __name__ == "__main__":
    booking = Booking().get_reviews()
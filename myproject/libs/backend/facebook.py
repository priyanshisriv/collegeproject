import requests
import re

from django.conf import settings
from fbs_project.libs.exceptions import *
from fbs_project.apps.reviews.models import Review

class Facebook:

    AUTH_URL = "https://www.facebook.com/dialog/oauth"
    LONG_LIVE_ACCESS_TOKEN = "https://graph.facebook.com/oauth/access_token"
    ACCOUNTS_URL = "https://graph.facebook.com/me/accounts/?access_token={0}&fields=username,access_token,name"
    PAGE_REVIEW = "https://graph.facebook.com/v3.1/{0}/ratings?fields=open_graph_story,reviewer&access_token={1}"
    REVIEW_COMMENT = "https://graph.facebook.com/v3.1/{0}/comments?message={1}&access_token={2}"
    GET_REVIEW_COMMENT = "https://graph.facebook.com/v3.1/{0}/comments?fields=message&access_token={1}"

    CLIENT_ID = settings.SOCIAL_AUTH_FACEBOOK_KEY
    CLIENT_SECRET = settings.SOCIAL_AUTH_FACEBOOK_SECRET
    SCOPE = settings.SOCIAL_AUTH_FACEBOOK_SCOPE

    def __init__(self, **kwargs):

        self.access_token = kwargs.get('access_token', None)
        self.link_instance = kwargs.get('link_instance', None)
        self.link = self.link_instance.link if self.link_instance else None
        self.page_list = []

    def update_link_instance(self):
        if self.link_instance:
            self.link_all_branch()

    def check_permission_is_allowed():
        #GET /{user-id}/permissions
        pass

    def exchange_token_from_code(self, access_token, extra_data=None):
        data = {
            'client_id': self.CLIENT_ID,
            'client_secret': self.CLIENT_SECRET,
            'grant_type': "fb_exchange_token",
            "fb_exchange_token": access_token
        }

        response = requests.get(self.LONG_LIVE_ACCESS_TOKEN, params=data)
        if response.status_code != 200:
            raise FacebookInvalidToken
        self.social_json  = response.json()
        if self.link_instance:
            self.update_link_instance()
        return self.social_json

    def is_link_ownership(self, **kwargs):
        account_url = self.ACCOUNTS_URL.format(self.access_token)
        page_access_token, page_id = self.get_page_access_token(account_url)
        if page_access_token:
            self.update_link_instance()
            return True
        else:
            return False

    def get_page_access_token(self, url = None):
        if not url:
            url = self.ACCOUNTS_URL.format(self.refresh_token)
        else:
            self.refresh_token

        page_id = self.link_instance.props.get("page_id", None)
        if not page_id:
            from fbs_project.libs.scrapper import get_page_id
            page_id = get_page_id(self.link_instance).get("page_id", None)
            self.link_instance.props['page_id'] = page_id
            self.link_instance.save()
        
        response = requests.get(url)
        json_respons = response.json()
        try:
            page_list = json_respons['data']
        except:
            raise FacebookInvalidToken
        self.page_list.extend(page_list)
        for page in page_list:
            if 'id' in page and page['id'] == page_id:
                return page['access_token'], page['id']
            else:
                if 'paging' in json_respons and 'next' in json_respons['paging']:
                    return self.get_page_access_token(json_respons['paging']['next'])
        return None, None

    def link_all_branch(self):
        from fbs_project.apps.business.models import Links
        links = Links.objects.filter(branch__business=self.link_instance.branch.business)

        page_id = self.link_instance.props.get("page_id", None)
        if not page_id:
            from fbs_project.libs.scrapper import get_page_id
            page_id = get_page_id(self.link_instance).get("page_id", None)

        for link in links:
            for page in self.page_list:
                if 'id' in page and page['id'] == page_id:
                    link.social_details = self.social_json
                    link.save()
                    break

    def get_page_reviews(self, url):
        """ facebook page reviews"""
        response = requests.get(url)
        reviews = []
        count = 0
        branch=self.link_instance.branch
        while count<=100:
            print("Count val", count)
            count += 1
            response = requests.get(url)
            if response.status_code != 200:
                print(response.json())
                raise FacebookInvalidToken
            else:
                json_respons = response.json()
                reviews += json_respons['data']
                if len(reviews) > 1:
                    latest_review = reviews[-1]["open_graph_story"]['id']
                    if Review.objects.filter(extra_data__id=latest_review,branch=branch).exists():
                        break
                if 'paging' in json_respons and 'next' in json_respons['paging']:
                    url = json_respons['paging']['next']
                    self.link_instance.props['next'] = url
                    self.link_instance.save()
                else:
                    self.link_instance.props['next'] = None
                    self.link_instance.save()
                    break
        return reviews

    @property
    def refresh_token(self):
        refresh_token = self.exchange_token_from_code(self.access_token)
        self.social_json = refresh_token
        return refresh_token['access_token']

    def get_rating_value(self, graph_data):
        if 'rating' in graph_data:
            return graph_data['rating'].get('value', 0)
        else:
            if 'recommendation_type' in graph_data and graph_data['recommendation_type'] == 'positive':
                return 5
            else:
                return 1

    def get_reviewer_data(self, graph_data):
        reviewer = dict()

        if 'reviewer' in graph_data:
            reviewer['name'] = graph_data['reviewer'].get('name', "")

        return reviewer
    
    # def get_comment(self, review_id, page_access_token):
    #     page_comment_url = self.GET_REVIEW_COMMENT.format(review_id, page_access_token)
    #     comment_data = self.get_page_reviews(page_comment_url)
    #     formatted_data = []
    #     for data in comment_data:
    #         comment = dict()
    #         comment['review'] = review_id
    #         comment['comments'] = data.get('message')
    #         comment['comment_create'] = data.get('created_time')
    #         formatted_data.append(comment)
    #     return formatted_data

    def format_data(self, reviews, page_access_token):
        """ reviews  format according to our system """

        formatted_data = list()

        for review in reviews:
            data = dict()
            graph_data = review['open_graph_story']
            data['description'] = graph_data.get('message')
            data['rating'] = self.get_rating_value(graph_data['data'])
            data['reviewer'] = self.get_reviewer_data(review)
            # data['reviewReply'] = self.get_comment(graph_data['id'], page_access_token)
            data['review_create'] = graph_data['start_time']
            data['extra_data'] = {'id': graph_data['id']}
            formatted_data.append(data)
        return formatted_data

    def get_reviews(self):
        """ """
        print("started")
        page_access_token, page_id = self.get_page_access_token()
        if not page_access_token:
            raise FacebookInvalidPageLink
        if self.link_instance.props.get('next'):
            page_review_url = self.link_instance.props.get('next')
        else:
            page_review_url = self.PAGE_REVIEW.format(page_id, page_access_token)
        print("initiating counter")  
        reviews_data = self.get_page_reviews(page_review_url)
        print('extracted page reviews', len(reviews_data))
        formatted_data = self.format_data(reviews_data, page_access_token)
        return formatted_data

    def post_comment_on_social_network(self, comment, **review_extra_data):

        """ post comment """
        review_id = review_extra_data['id']
        page_access_token, page_id = self.get_page_access_token()
        url = self.REVIEW_COMMENT.format(review_id, comment, page_access_token)
        response = requests.post(url)
        if response.status_code != 200:
            raise FacebookInvalidToken

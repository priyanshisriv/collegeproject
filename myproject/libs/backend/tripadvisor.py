import datetime
import requests


class TripAdvisor:
    """ Trip advisor review data"""

    EMAIL_REQUEST = "http://rcp-demo.ext.tripadvisor.com/api/partner/1.0/email_requests?key=693BC7CCA61444DB826640500AC319CC"

    def __init__(self,*args, **kwargs):
        self.API_KEY = "693BC7CCA61444DB826640500AC319CC"

    def get_location_list(self):
        """ get location list of given data """

        resposnse = requests.get()

    def create_request_email(self, recipient):
        """ send email request to trip adviser """

        data = {
            "recipient": "john@example.com",
            "location_id": "730099",
            "checkout": str(datetime.datetime.now().date()),
            "country": "US"
        }

        response = requests.post(self.EMAIL_REQUEST, json=[data])
        print(response.text)
        print(response.status_code)

    def get_email_request_list(self, location=730099):
        """ get the email request list api """

        data = {
            "location_id":location
        }

        response = requests.get(self.EMAIL_REQUEST, params=data)

        print(response.json())

        print(response.status_code)



if __name__ == "__main__":
    # TripAdvisor().create_request_email("amankumar0511@gmail.com")
    TripAdvisor().get_email_request_list()
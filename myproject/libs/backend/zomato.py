"""
@author: Aman Kumar
"""
import datetime
import pytz
import requests

from urllib import parse


class Zomato:

    API_KEY = "1b7be8cf980c3de936d2ee851bd7b1f0"
    PAGE_SZIE = 10

    URL = "https://developers.zomato.com/api/v2.1/reviews"

    def __init__(self, *args, **kwargs):
        """ """
        self.link_instance = kwargs.get('link_instance', None)
        self.link = self.link_instance.link if self.link_instance else kwargs.get('link')

    def format_data(self, reviews):
        """" format data to model dict """
        formatted_data = list()
        for review in reviews:
            data = dict()
            data['description'] = review['review'].get('review_text', '')
            data['rating'] = review['review'].get('rating', 0)
            data['reviewer'] = {
                'name': review['review']['user'].get("name", '')
            }

            data['review_create'] = datetime.datetime.fromtimestamp(review['review']['timestamp'])
            data['extra_data'] = {'id': review['review']['id']}
            formatted_data.append(data)
        return formatted_data

    def get_resturant_id(self, url):
        resturant_id = parse.parse_qs(parse.urlparse(url).query)['zrp_bid'][0]
        return resturant_id

    def get_reviews(self):

        res_id = self.get_resturant_id(self.link)

        headers = {'Accept': 'application/json', 'user-key': self.API_KEY}
        response = requests.get(self.URL + "?res_id=" + str(res_id), headers=headers)
        reviews = response.json()
        reviews = self.format_data(reviews['user_reviews'])
        return reviews


if __name__ == "__main__":
    zomato = Zomato(
        link="https://www.zomato.com/ncr/qla-mehrauli-new-delhi?zrp_bid=249202&zrp_pid=1&zrp_cid=333936&ref_id=32&ref_type=subzone")

    print(zomato.get_reviews())

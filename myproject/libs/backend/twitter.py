import re
import tweepy

from django.conf import settings
from django.core.cache import cache


from urllib import parse
from tweepy import OAuthHandler

from fbs_project.libs.exceptions import *
from fbs_project.libs.utils import get_text_sentiment

class Twitter:
    """ """
    request_token_url = 'http://twitter.com/oauth/request_token'
    access_token_url = 'http://twitter.com/oauth/access_token'
    authorize_url = 'http://twitter.com/oauth/authorize'



    def __init__(self, *args, **kwargs):
        """ social auth """
        consumer_key = '7LcW6fQO8mbH8u3sDtHNclHGO'
        consumer_secret = '5mAYIWQzbkrf6dq6OCh9v8cq9xuxsIRnpI3ze98aFqK9mLd6Fb'
        self.auth = OAuthHandler(consumer_key, consumer_secret, callback=settings.SOCIAL_AUTH_TWITTER_REDIRECT_URI)

    def get_oauth_flow_url(self):
        return self.auth.get_authorization_url(), self.auth.request_token

    def exchange_token_from_code(self, code, extra_data):
        """ exchange token from code """
        key = extra_data['user'].id
        key = "twitter-{0}".format(key)
        self.auth.request_token = cache.get(key)
        data = dict()
        data['access_token'], data['access_token_secret'] = self.auth.get_access_token(code)
        return  data


class TwitterClient(object):
    '''
    Generic Twitter Class for sentiment analysis.
    '''

    def __init__(self, **kwargs):

        '''
        Class constructor or initialization method.
        '''
        print(kwargs)
        # keys and tokens from the Twitter Dev Console
        consumer_key = '7LcW6fQO8mbH8u3sDtHNclHGO'
        consumer_secret = '5mAYIWQzbkrf6dq6OCh9v8cq9xuxsIRnpI3ze98aFqK9mLd6Fb'
        self.link_instance = kwargs['link_instance']
        self.link = self.link_instance.link
        self.auth = OAuthHandler(consumer_key, consumer_secret, callback=settings.SOCIAL_AUTH_TWITTER_REDIRECT_URI)
        try:
            self.auth.set_access_token(
                kwargs['access_token'],
                kwargs['access_token_secret']
            )
            self.api = tweepy.API(self.auth)
        except:
            raise InvalidTwitterAuth
        self.since_id = kwargs.get('since_id', None)
        self.reviews = list()


    def get_tweet_sentiment(self, tweet):
        return get_text_sentiment(tweet)

    def get_query(self):
        try:
            return "@" + self.link.split('/')[3]
        except:
            raise InvalidTwitterLink

    def update_link_instance(self, since_id):

        self.link_instance.props['since_id'] = since_id
        self.link_instance.save()

    def get_reviews(self, count=100, max_id=None):
        '''
        Main function to fetch tweets and parse them.
        '''
        # empty list to store parsed tweets
        query = self.get_query()
        since_id = self.since_id
        reviews = list()
        twitter_key_dict_map = dict()
        while True:
            try:
                # call twitter api to fetch tweets
                fetched_tweets = self.api.search(q=query, count=count, max_id=max_id, since_id=since_id)

                # parsing tweets one by one
                for tweet in fetched_tweets:
                    # empty dictionary to store required params of a tweet
                    parsed_tweet = {}
                    # saving text of tweet
                    parsed_tweet['text'] = tweet.text
                    parsed_tweet['id'] = tweet.id
                    parsed_tweet['created_at'] = tweet.created_at.isoformat()
                    parsed_tweet['reply_to'] = tweet.in_reply_to_screen_name
                    parsed_tweet['author'] = tweet.author.screen_name
                    # saving sentiment of tweet
                    parsed_tweet['sentiment'] = self.get_tweet_sentiment(tweet.text)
                    parsed_tweet['user'] = {
                        'name': tweet.user.name,
                        'image': tweet.user.profile_image_url
                    }
                    # appending parsed tweet to tweets list
                    # if tweet has retweets, ensure that it is appended only once
                    if parsed_tweet['id'] not in twitter_key_dict_map:
                        reviews.append(parsed_tweet)
                        twitter_key_dict_map[parsed_tweet['id']] = True
                    else:
                        print("duplicate data", parsed_tweet['id'])
                query_def = parse.parse_qs(parse.urlparse(fetched_tweets.next_results).query)
                try:
                    max_id = query_def['max_id'][0]
                    # break
                except (KeyError, IndexError):
                    if reviews:
                        since_id = reviews[0]['id']
                    else:
                        since_id = since_id
                    self.update_link_instance(since_id)
                    break
            except tweepy.TweepError as e:
                # print error (if any)
                    print("Error : " + str(e))
        print(reviews)
        return reviews

    def post_comment_on_social_network(self, comment, **review_extra_data):
        """ post review data """
        reply_status = "@%s %s" % (review_extra_data['author'], comment)
        try:
            respone = self.api.update_status(status=reply_status, in_reply_to_status=review_extra_data['id'])
        except:
            raise InvalidTwitterAuth

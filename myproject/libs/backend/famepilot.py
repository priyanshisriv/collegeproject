'''
@Author: Aman Kumar
'''

from fbs_project.libs.email import send_comment_email
from fbs_project.libs.sms import send_comment_sms


class Famepilot:
    """ famepilot related service"""

    def __init__(self, **kwargs):

        self.review = kwargs['review']

    def post_comment_on_social_network(self, comment, **kwargs):
        """ sms and mail user """

        send_comment_email(comment, self.review)

        send_comment_sms(comment, self.review)

import httplib2
import sys, json
import requests
import json

from django.conf import settings

from oauth2client.client import OAuth2WebServerFlow, OAuth2Credentials
from oauth2client.client import  HttpAccessTokenRefreshError
from apiclient.discovery import build

from fbs_project.libs.utils import get_the_place_id
from fbs_project.libs import exceptions as cus_exceptions


class GoogleClinet:

    """ google client for auth url """

    def __init__(self):
        self.flow = OAuth2WebServerFlow(
            client_id=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY,
            client_secret=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET,
            scope= settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE,
            redirect_uri=settings.SOCIAL_AUTH_GOOGLE_OAUTH2_REDIRECT,
            prompt="consent",
            access_type="offline"
        )

    def get_oauth_flow_url(self):
        """ call the auth flow for the user """

        auth_uri = self.flow.step1_get_authorize_url()

        return auth_uri, None

    def exchange_token_from_code(self, code, extra_data=None):

        """ get the exchange token from the page """
        url ="https://www.googleapis.com/oauth2/v4/token"
        data = {
            "code":code,
            "client_id":settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY,
            "client_secret": settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET,
            "grant_type" : "authorization_code",
            "redirect_uri": settings.SOCIAL_AUTH_GOOGLE_OAUTH2_REDIRECT
        }

        response = requests.post(url, data)
        return response.json()


class GoogleServie:

    def __init__(self, **kwargs):
        key = ['access_token', 'refresh_token',
                'token_expiry', 'token_uri', 'user_agent', 'revoke_uri',
                'id_token', 'token_response', 'scopes',
                'token_info_uri', 'id_token_jwt']
        credentials = dict()
        for i in key:
            credentials[i] = kwargs.get(i, None)
        credentials['client_id'] = settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY
        credentials['client_secret'] = settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET
        credentials['token_uri']="https://accounts.google.com/o/oauth2/token"
        self.credentials = OAuth2Credentials(**credentials)
        self.link_instance = kwargs.get('link_instance', None)

    def update_link_instance(self, props):

        self.link_instance.props = props
        self.link_instance.save()

    def authrizer_httplib2(self):
        """ authorize """
        http = httplib2.Http()
        http = self.credentials.authorize(http)
        return http

    def get_account(self, service):
        list_accounts_response = service.accounts().list().execute()
        account = list_accounts_response['accounts'][0]['name']
        return account

    def get_location(self):
        """ get the location """
        return self.link_instance.props.get('location_id', None)

    def get_address(self, location):
        address = str()
        address += location.get('locationName', "")
        address += ",".join(location['address']['addressLines'])
        address += location['address']['locality']
        return address

    def format_links(self, links):
        """ format data """
        formatted_data = list()
        for link in links:
            link_data = dict()
            link_data['address'] = self.get_address(link)
            link_data['location_id'] = link['name']

            try:
                link_data['review_link'] = link['metadata']['newReviewUrl']
            except KeyError:
                continue
            formatted_data.append(link_data)
        return formatted_data

    def get_all_links(self):
        """ get all links of the user account """
        links = list()
        http = self.authrizer_httplib2()
        service = build(
            'mybusiness',
            'v4.1',
            http=http,
            cache_discovery=False,
            discoveryServiceUrl="https://developers.google.com/my-business/samples/mybusiness_google_rest_v4p1.json"
        )
        account = self.get_account(service)
        request = service.accounts().locations().list(parent=account)
        while request is not None:
            locations_list = request.execute()
            for location in locations_list.get('locations', []):
                links.append(location)
            request = service.accounts().locations().list_next(request, locations_list)
        formatted_data = self.format_links(links)
        return formatted_data

    def is_link_ownership(self, **kwargs):
        http = self.authrizer_httplib2()
        service = build(
            'mybusiness',
            'v4.1',
            http=http,
            cache_discovery=False,
            discoveryServiceUrl="https://developers.google.com/my-business/samples/mybusiness_google_rest_v4p1.json"
        )

        account = self.get_account(service)
        location = self.get_location(service, account, kwargs['location_name'])

        if location:
            return True
        else:
            return False

    def get_reviews(self, **kwargs):
        http = self.authrizer_httplib2()
        service = build(
            'mybusiness',
            'v4.1',
            http=http,
            discoveryServiceUrl="https://developers.google.com/my-business/samples/mybusiness_google_rest_v4p1.json"
        )

        location = self.get_location()
        if not location:
            raise cus_exceptions.GoogleLocationNotFound

        if self.link_instance.props.get("nextPageToken"):
            request = service.accounts().locations().reviews().list(
                parent=location,
                orderBy='rating',
                pageToken=self.link_instance.props.get("nextPageToken")
            )
        else:
            request = service.accounts().locations().reviews().list(parent=location, orderBy='rating')
        reviews = []
        while request is not None:
            reviews_list = request.execute()
            if reviews_list.get('reviews'):
                reviews += reviews_list['reviews']
            if reviews_list.get('nextPageToken'):
                self.link_instance.props.update({"nextPageToken": reviews_list.get('nextPageToken')})
                self.link_instance.save()
            request = service.accounts().locations().reviews().list_next(request, reviews_list)

        return reviews

    def post_comment_on_social_network(self, body, **kwargs):

        http = self.authrizer_httplib2()
        service = build(
            'mybusiness',
            'v4.1',
            http=http,
            discoveryServiceUrl="https://developers.google.com/my-business/samples/mybusiness_google_rest_v4p1.json"
        )
        service.accounts().locations().reviews().updateReply(name=kwargs['name'], body={'comment':body}).execute()

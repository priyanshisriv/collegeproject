import requests
import re

from fbs_project.libs.exceptions import InvalidGoibiboLink


class Goibibo:

	REVIEW_URL = "http://ugc.goibibo.com/api/HotelReviews/forWeb"
	APPLICATION_ID = "b95bd093"
	APPLICATION_KEY = "d2df1e9571d2581e63c32dea1bfee31a"

	def __init__(self, *args, **kwargs):
		self.link_instance = kwargs.get('link_instance', None)
		self.link = self.link_instance.link if self.link_instance else kwargs.get('link', None)

	def get_location_id(self):
		if self.link:
			try:
				link_word = self.link.split("/")
				hotel_name = link_word[4]
				id_list = re.findall('\d+', hotel_name)
				return id_list[-1]
			except KeyError:
				raise InvalidGoibiboLink
		else:
			raise InvalidGoibiboLink

	def format_data(self, reviews):
		"""" format data to model dict """
		formatted_data = list()
		for review in reviews:
			data = dict()
			data['description'] = review.get('reviewContent', '')
			data['rating'] = review.get('totalRating', 0)
			data['reviewer'] = {
				'name': review["reviewer"].get('firstName', "") + " "+ review["reviewer"].get('lastName', "")
			}
			data['review_create'] = review['createdAt']
			data['extra_data'] = {'id':review['id']}
			formatted_data.append(data)
		return formatted_data

	def get_reviews(self, **kwargs):
		""" get reviews for the hotel """

		data = {
			'app_id': self.APPLICATION_ID,
			'app_key': self.APPLICATION_KEY,
			'vid': self.get_location_id()
		}
		response = requests.get(self.REVIEW_URL, params=data)
		reviews = response.json()
		reviews = self.format_data(reviews)

		return reviews


if __name__ == "__main__":
	
	goibibo = Goibibo(link="https://www.goibibo.com/hotels/asian-suites-a-585-hotel-in-gurgaon-5361191673431822820/")


	print(goibibo.get_reviews())